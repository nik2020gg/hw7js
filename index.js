function filterBy(arr, dataType) {
  return arr.filter((item) => typeof item !== dataType);
}

// Приклад використання:
const inputArray = ["hello", "world", 23, "23", null];
const dataTypeToFilter = "string";

const resultArray = filterBy(inputArray, dataTypeToFilter);

console.log(resultArray); // Результат: [ 23, null ]
